<?php

	if(!empty($_REQUEST))
	{
		sleep(2);
	}
?>
<?php if(isset($_REQUEST['sidebar']) && $_REQUEST['sidebar'] === 'fail') : ?>
	<div id="myform" data-response='{"action":{"open-overlay":{"path": "#popup"}}}'>
		<h4>Something terrible has happened, please try again later</h4>
	</div>
<?php elseif(isset($_REQUEST['sidebar']) && $_REQUEST['sidebar'] === 'pass') : ?>
	<div id="myform" data-response='{"action":{"close-overlay":{}, "page-refresh":{"delay":500}}}'>
		PASS
	</div>
	<div class="alert alert--sticky alert--actionable" data-alert='{"delay": 0}'>
		<div class="bg"></div>
		<span class="alert__bar"><span></span></span>
		<div class="container container--md alert__container">
			<button class="ui ui--close" data-close>
				<i aria-hidden="true" class="icon icon--cross icon--sm"><svg><use xlink:href="../../icon/dist/icons.svg#cross"></use></svg></i>
			</button>
			<div class="content alert__content">
				<!-- media -->
				<div class="media media--top">
					<div class="media__figure">
						<img src="http://placehold.it/60" alt="">
					</div>
					<div class="media__body">
						<p>You've been signed out of your account</p>
					</div>
				</div>
				<!-- /media -->
			</div>
		</div>
		<footer class="alert__footer">
			<div class="container container--md alert__container">
				<div class="content alert__content">
					<div class="grid grid--gutters grid--2@sm">
						<div class="grid__item">
							<a href="#" class="btn btn--primary btn--tertiary btn--wide">Continue shopping</a>
						</div>
						<div class="grid__item">
							<a href="#" class="btn btn--primary btn--secondary btn--wide">Sign in now</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
<?php else : ?>
<form id="myform" action="form.php">
	<input type="text" name="sidebar" value="<?php print isset($_REQUEST['sidebar']) ? $_REQUEST['sidebar'] : ''; ?>">
	<button class="btn btn--wide btn--primary">Submit</button>
</form>
<?php endif; ?>
