<html>
	<head>
		<title>Overlays</title>
		<link rel="stylesheet" href="../../normalize/dist/normalize.css">
		<link rel="stylesheet" href="../../base/dist/base.css">
		<link rel="stylesheet" href="../../typography/dist/typography.css">
		<link rel="stylesheet" href="../../layout/dist/container.css">
		<link rel="stylesheet" href="../../icon/dist/icon.css">
		<link rel="stylesheet" href="../../ui/dist/ui.css">
		<link rel="stylesheet" href="../../button/dist/button.css">
		<link rel="stylesheet" href="../../alert/dist/alert.css">
		<link rel="stylesheet" href="../../layout/dist/media.css">
		<link rel="stylesheet" href="../../layout/dist/grid.css">
		<link rel="stylesheet" href="../dist/overlay.css">
		<style>
			div[aria-hidden="true"]
			{
				display						: none;
			}
		</style>
	</head>
	<body>
		<div class="container">

			<br>
			<a href="#side1" class="btn btn--primary js-overlay-o">Side</a>
			<a href="#popup" class="btn btn--primary js-overlay-o">Popup</a>

		</div>
		<div class="container container--padded container--md">
			<!-- card -->
			<div id="side1" data-title="Side 1" aria-hidden="true">
				<div class="card card--padded">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore debitis, nemo error dicta harum aut unde fuga, adipisci blanditiis est rerum, beatae minima libero ab temporibus eaque cupiditate sed distinctio!</p>

					<?php include "form.php"; ?>

					<a href="#side2" class="btn btn--positive js-overlay-o">Side 2</a>
				</div>
			</div>
			<div id="side2" data-title="Side 2" aria-hidden="true">
				<div class="card card--padded">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore debitis, nemo error dicta harum aut unde fuga, adipisci blanditiis est rerum, beatae minima libero ab temporibus eaque cupiditate sed distinctio!</p>
					<a href="#side3" class="btn btn--positive js-overlay-o">Side 3</a>
				</div>
			</div>
			<div id="side3" data-title="Side 3" aria-hidden="true">
				<div class="card card--padded">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore debitis, nemo error dicta harum aut unde fuga, adipisci blanditiis est rerum, beatae minima libero ab temporibus eaque cupiditate sed distinctio!</p>
				</div>
			</div>
			<div id="popup" data-title="Popup" aria-hidden="true" data-overlay='{"modal":true}'>
				<div class="card card--padded">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore debitis, nemo error dicta harum aut unde fuga, adipisci blanditiis est rerum, beatae minima libero ab temporibus eaque cupiditate sed distinctio!</p>
				</div>
			</div>
		</div>
		<div id="overlays">
			<div class="overlay" role="dialog">
				<div class="container container--padded overlay__container">
					<div class="overlay__header">
						<h2 class="h4 overlay__title">[Overlay Title]</h2>
						<a href="#" role="button" class="ui ui--close overlay__close js-overlay-c">
							<i aria-hidden="true" class="icon icon--cross icon--sm"><svg><use xlink:href="../../icon/dist/icons.svg#cross"></use></svg></i>
						</a>
					</div>
					<div class="overlay__body"></div>
				</div>
			</div>
		</div>

		<script src="../dist/overlay.js"></script>
	</body>
</html>
