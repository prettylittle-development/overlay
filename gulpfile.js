
var gulp						= require('gulp'),
	sass						= require('gulp-sass');

gulp.task('styles', function()
{
	gulp.src('./src/*.scss')
		.pipe(sass
			(
				{
					outputStyle		: "expanded",
					errLogToConsole	: true,
					indentType 		: 'tab',
					indentWidth		: 1,
				}
			)
		)
		.pipe(gulp.dest('./dist'));
});

var source						= require('vinyl-source-stream'),
	browserify					= require('browserify'),
	babel						= require('babelify'),
	notify						= require('gulp-notify'),
	plumber						= require('gulp-plumber'),
	watch						= require('gulp-watch'),
	rename						= require('gulp-rename'),
	uglify						= require('gulp-uglify');

gulp.task('scripts', function()
	{
		var bundler	= browserify('./src/overlay.js',
			{
				debug			: false
			}
		).transform
		(
			babel.configure
			(
				{
					// Use all of the ES2015 spec
					presets		: ["es2015"]
				}
			)
		);

		var plumberErrorHandler	= {
			errorHandler	:notify.onError(
				{
					title		:"Gulp",
					message		:"Error: <%= error.message %>"
				}
			)
		};

		return bundler.bundle()
			.pipe(plumber(plumberErrorHandler))
			//
			.pipe(source('overlay.js'))
			// Save off the uncompressed JS
			.pipe(gulp.dest('dist'))
			/*
			// Rename the output file to a minified extension
			.pipe(rename({extname:'.min.js'}))
			// Compress the JS
			.pipe(uglify(
				{
					// http://lisperator.net/uglifyjs/compress
					compress :
					{
						global_defs :
						{
							DEBUG		: false
						}
					}
				}
			))
			// Save off the minified file
			.pipe(gulp.dest('dist'));
			*/
	}
);

gulp.task('scripts:watch', function()
{
	watch(['./*.js', 'src/*.js'], function(vinyl)
	{
		gulp.start.apply(gulp, ['scripts']);
	});
});

gulp.task('styles:watch', function()
	{
		watch(['./*.scss', 'src/*.scss'], function(vinyl)
		{
			gulp.start.apply(gulp, ['styles']);
		}
	);
});

gulp.task('default', ['scripts', 'styles', 'styles:watch', 'scripts:watch']);

