
import {events} from '../../events/events';
import {dom} from '../../dom/dom';
import EventManager from '../../events/EventManager';
import overlay from '../overlay';
import ajax from '../../form/ajax';
import popup from '../../alert/alert';

EventManager.add_action('init_content', [popup, 'init']);
EventManager.add_action('init_content', [overlay, 'init']);
EventManager.add_action('init_content', [ajax, 'init']);

events.ready(function()
{
	EventManager.do_action('init_content');
});

