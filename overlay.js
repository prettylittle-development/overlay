
import {dom} from '../dom/dom';
import {events} from '../events/events';
import {ElementData} from '../dom/ElementData';
import Loader from '../request/Loader';
import EventManager from '../events/EventManager';

let overlay =
{
	init : function(root = document)
	{
		const params =
		{
			overlay_id			: 'overlays',
			inactive_class		: 'inactive',
			active_class		: 'active',
			data_name			: 'overlay',
			default_id			: "#content"
		};

		const KEY_ESC			= 27;
		const KEY_TAB			= 9;

		// if there is no overlay background div on the page, add one
		if(!document.getElementById(params.overlay_id))
		{
			var bg		= document.createElement('div');
				bg.id	= params.overlay_id;

			document.body.appendChild(bg);
		}

		// global
		let overlayHolder		= document.querySelector('#' + params.overlay_id);
		let template			= overlayHolder.innerHTML;

		// used for looking up overlays
		let overlay_dictionary	= {};

		// an array to hold all the overlays
		let overlays			= [];
		let overlays_open		= [];

		let last_focused		= [];

		function get_id_from_url(url)
		{
			let id				= url.toLowerCase();

			// TODO: make this relative (or absolute)

			return id;
		}

		function new_create_overlay(overlay, url)
		{
			//console.info('new_create_overlay');

			// default settings for the overlay
			let defaults			=
			{
				// public
				"title"				: '',
				"classes"			: '',
				"dismissible"		: true
			};

			// the the json embedded alert data from the overlay
			let options				= ElementData.get(params.data_name, overlay, defaults);

			try
			{
				let wrapper			= dom.create(template).children[0];

				// remove the attribute from the loaded overlay
				overlay.removeAttribute('data-' + params.data_name);

				// add the overlay details onto the wrapper (new overlay)
				ElementData.set(params.data_name, wrapper, options);

				// find the template body
				let body			= dom.find(wrapper, '[data-overlay-body]');

				if(options.dismissible == false)
				{
					let closer		= dom.find(wrapper, '[data-overlay-close]');

					[].forEach.call(closer, function(close, i)
					{
						dom.remove(close);
					});
				}

				// remove any aria hidden values from the overlay
				overlay.setAttribute('aria-hidden', false);

				// add the newly loaded html into the template
				dom.append(body[0], overlay);

				for(var i in options)
				{
					var el				= dom.find(wrapper, '[data-overlay-' + i + ']');

					if(el.length)
					{
						el[0].innerHTML	= options[i];
					}
				}

				if(options['classes'])
				{
					dom.addClass(wrapper, options['classes']);
				}

				// add the overlay into the overlay holder
				dom.append(overlayHolder, wrapper);

				// allow other hools to hook into the overlay and init content
				EventManager.do_action('init_content', overlay);

				// add the new overlay into the overlays list
				overlays.push(wrapper);

				// get the overlay id form the url
				let id					= get_id_from_url(url);

				// the overlay id is based in the order that it is in the overlays list
				let overlay_id			= overlays.length - 1;

				// update the dictionary lookup to a specific id for the overlay
				overlay_dictionary[id]	= overlay_id;

				new_open_overlay(overlay_id);
			}
			// if anything went wrong, notify the user/dev
			catch(e)
			{
				console.error(e);
			}
		}

		function new_open_overlay(overlay_number)
		{
			//console.info('new_open_overlay', overlay_number);

			// TODO: additional checks to make sure that the overlay is not already open

			overlays_open.push(overlay_number);

			// get the overlay
			let overlay				= overlays[overlay_number];

			// make sure that the overlay is at the top
			overlay.style.zIndex	= overlays_open.length + 1;

			setTimeout(function()
			{
				new_position_overlays();

				dom.addClass(overlayHolder, params.active_class);
				dom.addClass(overlay, params.active_class);

				setTimeout(function()
				{
					// TODO - implement
					// make the modal/overlay accessible
					//var focusable_els		= overlay.querySelectorAll('a[href]:not(.ui), area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), [tabindex="0"]');
					let focusable_els		= overlay.querySelectorAll('input:not([disabled]):not([type="hidden"]), select:not([disabled]), textarea:not([disabled])');
						focusable_els		= Array.prototype.slice.call(focusable_els);

					if(focusable_els.length)
					{
						// TODO - this breaks functionality
						focusable_els[0].focus();
					}

				}, 300);

			}, 0);

			// make the page not scrollable
			dom.addClass(document.body, 'noscroll');
		}

		function new_load_overlay(overlay_url)
		{
			//console.info('new_load_overlay');

			// split the url into parts based in the hash
			let parts			= overlay_url.split('#');

			// load the url, and get the content
			new_get_dom('#' + parts[1], parts[0], function(xhr, el, url)
			{
				// Success callback
				new_create_overlay(el, overlay_url);

			}, function(xhr, el, url)
			{
				// Fail callback
				window.location	= xhr.responseURL;

			}, function()
			{
				// Progress callback
			});
		}

		function new_position_overlays()
		{
			//console.info('new_position_overlays');

			[].forEach.call(overlays_open, function(id, i)
			{
				var overlay		= overlays[id];

				// offset to stagger the overlays
				var offset		= (overlays_open.length - i - 1) * 30;
				var is_active	= i === (overlays_open.length - 1);

				if(!is_active)
				{
					dom.addClass(overlay, params.inactive_class);
				}
				else
				{
					dom.removeClass(overlay, params.inactive_class);
				}

				// add the margin to the overlay
				overlay.style.marginRight	= offset + 'px';
			});
		}

		function new_close_all()
		{
			while(overlays_open.length)
			{
				new_close_overlay(overlays_open[overlays_open.length - 1]);
			}
		}

		function new_close_overlay(overlay_id)
		{
			// TODO: get and remove from the open overlays, don't assume that the overlay is at the top
			overlays_open.pop(); // splice not pop

			// get the overlay based in the key int
			let overlay			= overlays[overlay_id];

			// remove the active class for the overlay
			dom.removeClass(overlay, params.active_class);

			// if there are no more overlays open
			if(overlays_open.length === 0)
			{
				// remove the active class from the overlay background
				dom.removeClass(overlayHolder, params.active_class);

				setTimeout(function()
				{
					dom.removeClass(document.body, 'noscroll');
				}, 10);
			}
			else
			{
				new_position_overlays();
			}
		}

		function new_get_dom(selector, url, success, fail, progress)
		{
			// create an instance of the ajax loader to load the content
			let _loader 			= new Loader();

			// set up success and error events
			_loader.get(url).success
			(
				function(data, xhr)
				{
					try
					{
						// create a div that will be hidden, so we can query inner content
						let hidden				= document.body.appendChild(document.createElement("div")),
							re					= new RegExp(/<body[^>]*>([\s\S]+)<\/body>/i);

						// hide the div
						hidden.style.display	= 'none';

						// match the content between the body tabs
						let match				= data.match(re);

						// if there are matches, use the inner HTML of the request in the hidden container
						if(match)
						{
							hidden.innerHTML	= match[1];
						}
						// fallback to use the entire request data
						else
						{
							hidden.innerHTML	= data;
						}

						// get the element from the hidden
						let elem				= hidden.querySelector(selector);

						// if there is a element that matched the selector, we have our overlay
						if(elem)
						{
							// TODO - pull any alerts out from the
							console.info("insert additional alerts/messages etc...");

							// trigger the success callback
							success(xhr, elem, url);
						}
						else
						{
							// trigger the fail callback
							fail(xhr, null, url);
						}

						// remove the hidden element
						document.body.removeChild(hidden);
					}
					// if anything failed...
					catch(e)
					{
						// trigger the fail callback
						fail(xhr, null, url);
					}
				}
			).error
			(
				function(data, xhr)
				{
					console.log('-> error', arguments);
				}
			).always
			(
				function(data, xhr)
				{
					//console.log('-> always', arguments);
				}
			);
		}

		function trigger_overlay(url)
		{
			//console.info('trigger_overlay');

			// if there is no hash of an element to pull in
			if(url.indexOf('#') === -1)
			{
				url			   += params.default_id;
			}

			let id				= get_id_from_url(url);

			// if the overlay has already been initialized
			if(overlay_dictionary[id] !== undefined)
			{
				let state		= overlay_dictionary[id];

				if(typeof(state) === 'string')
				{
					console.warn(`Overlay ${url} is ${state}`)
				}
				else
				{
					new_open_overlay(overlay_dictionary[id]);
				}
			}
			else
			{
				// if the url begins with a hash, the overlay should already exist on the page
				if(url.charAt(0) === '#')
				{
					let el		= document.querySelector(url);

					// if the element does exist, create the overlay for real
					if(el)
					{
						// register the fact that the overlay has been requested
						overlay_dictionary[id]	= "building";

						new_create_overlay(el, url);
					}
					else
					{
						console.warn(`Overlay ${url} does not exist on the page`);
					}
				}
				else
				{
					// register the fact that the overlay has been requested
					overlay_dictionary[id]	= "loading";

					new_load_overlay(url);
				}
			}

			// record the last focused element, so once the overlay is closed
			last_focused.push(document.activeElement);
		}

		EventManager.add_action('overlay/type=open', trigger_overlay);



		// trap click events
		events.live('[data-overlay-trigger]', 'click', function(ev)
		{
			let defaults		=
			{
				// public
				"close"			: false
			};

			let options			= ElementData.get('overlay-trigger', this, defaults);

			// if the ctrl key is not down when clicked
			if(!ev.ctrlKey || options['close'])
			{
				events.cancel(ev);

				// TODO: decide if this is an open or a close event
				if(options['close'])
				{
					if(overlays_open.length)
					{
						if(options['close'] === 'all')
						{
							new_close_all()
						}
						else
						{
							new_close_overlay(overlays_open[overlays_open.length - 1]);
						}
					}
				}
				else
				{
					trigger_overlay(this.getAttribute('href'));
				}
			}
		});

		EventManager.add_action('overlay/type=close', function()
		{
			if(overlays_open.length)
			{
				new_close_overlay(overlays_open[overlays_open.length - 1]);
			}
		});

		events.on(window, 'keydown', function(ev)
		{
			switch(ev.keyCode)
			{
				case KEY_TAB:

					//if there are open overlays
					if(overlays_open.length)
					{
						// get the top/active overlay
						let overlay				= overlays[overlays_open[overlays_open.length - 1]];

						//
						let focusableEls		= overlay.querySelectorAll('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), [tabindex="0"]');
    						focusableEls		= Array.prototype.slice.call(focusableEls);

    					if(focusableEls.length)
    					{
	    					let firstFocusableEl 	= focusableEls[0];
	    					let lastFocusableEl 	= focusableEls[focusableEls.length - 1];

							if(focusableEls.length === 1)
							{
								events.cancel(ev);
								break;
							}

							if(ev.shiftKey)
							{
								if (document.activeElement === firstFocusableEl)
								{
									events.cancel(ev);

									lastFocusableEl.focus();
								}
							}
							else
							{
								if (document.activeElement === lastFocusableEl)
								{
									events.cancel(ev);

									firstFocusableEl.focus();
								}
							}
						}
					}

					break;

				case KEY_ESC:

					if(overlays_open.length)
					{
						new_close_overlay(overlays_open[overlays_open.length - 1]);
					}

					break;
			}
		});

		events.on(overlayHolder, 'click', function(ev)
		{
			if((ev.target === overlayHolder || dom.containsClass(ev.target, 'dialog')) && overlays_open.length)
			{
				// get the overlay at the top
				let active			= overlays_open[overlays_open.length - 1];

				// get the actual html element for the overlay
				let overlay			= overlays[active];

				// if the overlay exists
				if(overlay)
				{
					// get the options defined for the overlay
					let options		= ElementData.get(params.data_name, overlay);

					// if the overlay is dismissible
					if(options.dismissible)
					{
						// close it
						new_close_overlay(active);
					}
				}
			}
		});

		if(window.location.hash)
		{
			let triggers		= document.querySelectorAll('a[href="' + window.location.hash + '"][data-overlay-trigger]');

			if(triggers.length)
			{
				triggers[0].click();
			}
		}

		//if(!overlayHolder)
		//{
		//	return false;
		//}

		/*

		var get_id = function(string)
		{
			string				= string.toLowerCase();

			// TODO - add more
			return string;
		}

		var pages				= [];
		var pages_open			= [];
		var lookups				= {};

			overlayHolder.innerHTML	= '';

		var position_overlays = function()
		{
			// TODO..
			[].forEach.call(pages_open, function(id, i)
			{
				var page		= pages[id];

				// offset to stagger the overlays
				var offset		= (pages_open.length - i - 1) * 30;
				var is_active	= i === (pages_open.length - 1);

				if(!is_active)
				{
					dom.addClass(page, params.inactive_class);
				}
				else
				{
					dom.removeClass(page, params.inactive_class);
				}

				// add the margin to the overlay
				page.style.marginRight	= offset + 'px';
			});
		}
		*/

/*
		var open_overlay = function(key)
		{
			// add the new page into the pages open array
			pages_open.push(key);

			// get the overlay from the pages
			var overlay				= pages[key];
			overlay.style.zIndex	= pages_open.length + 1;

			setTimeout(function()
			{
				position_overlays();

				dom.addClass(overlayHolder, params.active_class);
				dom.addClass(overlay, params.active_class);

			}, 100);

			dom.addClass(document.body, 'noscroll');
		}
		*/

		/*
		var create_overlay = function(xhr, el, url)
		{
			//
			// default settings for the alert
			let defaults		=
			{
				// public
				"title"			: '',
				"classes"		: '',
				"dismissible"	: true
			};

			// create an overlay wrapper from the template html already embedded in the page
			var overlay			= dom.create(template).children[0];

			// find the template body
			var body			= dom.find(overlay, '.overlay__body');

			el.setAttribute('aria-hidden', false);

			// add the newly loaded html into the template
			dom.append(body[0], el);

			// the the json embedded alert data from the popup/alert
			let options			= ElementData.get(params.data_name, el, defaults);

			console.log(options);

			var title				= dom.find(overlay, '.overlay__title');
				title[0].innerHTML	= defaults['title'];

			// add the overlay into the overlay holder
			dom.append(overlayHolder, overlay);

			// add the newly loaded overlay into the pages
			pages.push(overlay);

			EventManager.do_action('init_content', overlay);

			// what possition was the content added to the pages array
			var position			= pages.length - 1;

			// get the id from the url, so we can update the lookups data
			var id					= get_id(url);

			// update the lookups with a position
			lookups[id]				= position;

			// open the overlay...
			open_overlay(position);
		}

		var load = function(url, callback)
		{
			var parts			= url.split('#');
			var full_url		= url;

			getDOM('#' + parts[1], parts[0], function(xhr, el, url)
			{
				create_overlay(xhr, el, full_url);

				if(callback)
				{
					callback.call();
				}
			}, function(xhr, el, url)
			{
				window.location		= xhr.responseURL;

			}, function()
			{
				// progress
			});
		}

		var getDOM = function(selector, url, success, fail, progress)
		{
			var _loader 			= new Loader();

			// set up success and error events
			_loader.get(url).success
			(
				function(data, xhr)
				{
					var hidden				= document.body.appendChild(document.createElement("div")),
						re					= new RegExp(/<body[^>]*>([\s\S]+)<\/body>/i),
						elem;

					hidden.style.display	= 'none';
					hidden.innerHTML		= data.match(re)[1];

					elem					= hidden.querySelector(selector);

					// TODO - pull any alerts out from the
					console.info("insert additional alerts/messages etc...")

					if(true)
					{
						success(xhr, elem, url);
					}
					else
					{
						alert("FAIL");
					}

					document.body.removeChild(hidden);
				}
			).error
			(
				function(data, xhr)
				{
					console.log('-> error', arguments);
				}
			).always
			(
				function(data, xhr)
				{
					//console.log('-> always', arguments);
				}
			)
		}

		var _open = function(href)
		{
			// get an id to use as a lookup
			var id			= get_id(href);

			// if the overlay has already been created... there's no point in generating another overlay
			if(lookups[id] !== undefined)
			{
				// if the item has a state rather than an id number...
				if(typeof(lookups[id]) === 'string')
				{
					console.warn("STATE:" + lookups[id]);
				}
				else
				{
					// open the correct overlay
					open_overlay(lookups[id]);
				}
			}
			else
			{
				// set a loading stare for the overay, as it hasn't yet been created
				lookups[id]				= 'loading';

				// if the overlay is a link to an existing element on the page
				if(href.charAt(0) === '#')
				{
					var el				= document.querySelector(href);

					if(el)
					{
						create_overlay(null, el, href);
					}
					else
					{
						alert("inPage overlay not available...");
					}
				}
				else
				{
					load(href);
				}
			}
		}

		var open	= function(ev)
		{
			if(!ev.ctrlKey)
			{
				events.cancel(ev);

				// get the anchor url
				_open(this.getAttribute('href'));
			}
		}

		var close	= function(ev)
		{
			events.cancel(ev);

			var key				= pages_open.pop();
			var overlay			= pages[key];

			dom.removeClass(overlay, params.active_class);

			if(pages_open.length === 0)
			{
				dom.removeClass(overlayHolder, params.active_class);

				setTimeout(function()
				{
					dom.removeClass(document.body, 'noscroll');
				}, 10);
			}

			position_overlays();
		}

		var closeAll = function(ev)
		{
			events.cancel(ev);

			for(var i = 0; i < pages_open.length; i++)
			{
				var key				= pages_open.pop();
				var overlay			= pages[key];

				dom.removeClass(overlay, params.active_class);

				position_overlays();
			}

			if(pages_open.length === 0)
			{
				dom.removeClass(overlayHolder, 'overlays--active');
				dom.removeClass(document.body, 'noscroll');
			}
		}

		EventManager.add_action('close-overlay', function(options, element)
		{
			var defaults		= {
				delay			: 0
			};

			options				= Object.assign(defaults, options);

			setTimeout(function()
			{
				close();
			}, options.delay);

		}, 10, 2);

		EventManager.add_action('open-overlay', function(options, element)
		{
			var defaults		= {
				path			: '',
				delay			: 0
			};

			options				= Object.assign(defaults, options);

			setTimeout(function()
			{
				_open(options.path);
			}, options.delay);

		}, 10, 2);

		//
		events.live('.js-overlay-o', 'click', open);
		events.live('.js-overlay-c', 'click', close);
		events.live('.js-overlay-a', 'click', closeAll);

		if(window.location.hash)
		{
			var elements		= document.querySelectorAll('a[href="' + window.location.hash + '"]');

			[].forEach.call(elements, function(link, i)
			{
				if(dom.containsClass(link, 'js-overlay-o'))
				{
					link.click();
				}
			});
		}
		*/
	}
}

export default overlay;
