(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _dom = require('../dom/dom');

var _events = require('../events/events');

var _EventManager = require('../../libs/events/EventManager');

var _EventManager2 = _interopRequireDefault(_EventManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var progress = require('../progressbar/progressbar');

var popup = {
	init: function init() {
		var root = arguments.length <= 0 || arguments[0] === undefined ? document : arguments[0];

		var getConfig = function getConfig(name, el) {
			var defaults = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

			var attr = el.getAttribute('data-' + name),
			    options = {};

			if (attr) {
				try {
					options = JSON.parse(attr);
				} catch (e) {
					console.error('invalid config data *data-' + name + '*', el);
				}
			}

			return Object.assign(defaults, options);
		};

		var setConfig = function setConfig(name, el) {
			var changes = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

			var config = getConfig(name, el);

			config = Object.assign(config, changes);

			el.setAttribute("data-" + name, JSON.stringify(config));
		};

		// TODO: move this to another global file
		_events.events.live('a[data-action]', 'click', function (ev) {
			_events.events.cancel(ev);

			var options = getConfig('action', this);

			for (var action in options['actions']) {
				_EventManager2.default.do_action(options['actions'][action], ev);
			}
		});

		// TODO: add content loaded action

		var params = {
			data_name: 'alert',
			active_class: 'active',
			background_id: 'alert-background'
		};

		if (!document.getElementById(params.background_id)) {
			var bg = document.createElement('div');
			bg.id = params.background_id;

			_dom.dom.addClass(bg, params.background_id);

			document.body.appendChild(bg);
		}

		// get all of the alert items on the page
		var alerts = root.querySelectorAll('[data-' + params.data_name + ']');

		// loop through them
		[].forEach.call(alerts, function (popup) {
			// default settings for the alert
			var defaults = {
				// public
				"delay": false,
				"bg": false,
				"time": false,
				// private
				"_open": false,
				"_init": false
			};

			// the the json embedded alert data from the popup/alert
			var options = getConfig(params.data_name, popup, defaults);

			// if this alert has already been initialized...
			if (options._init) {
				return;
			}

			// add the popup to the body to make sure there's no overlapping and
			// z-index issues
			document.body.appendChild(popup);

			var alert_interval = null;

			/**
    * [close description]
    * @param  {[type]} ev [description]
    * @return {[type]}    [description]
    */
			var close = function close(ev) {
				// if an event had been passed in (assume this event was bound to a click event) cancel the event
				if (ev) {
					_events.events.cancel(ev);
				}

				// the alert is no longer open
				options._open = false;

				// remove the active class form the popup
				_dom.dom.removeClass(popup, params.active_class);

				// if there is a background displayed, hide it, and kill the noscroll
				if (options.bg) {
					_dom.dom.removeClass(document.body, 'noscroll');

					_dom.dom.removeClass(document.getElementById(params.background_id), params.active_class);
				}

				// if there is an overlay, kill it so it doesn't get executed
				if (alert_interval) {
					clearTimeout(alert_interval);
				}
			};

			/**
    * [open description]
    * @param  {[type]} ev [description]
    * @return {[type]}    [description]
    */
			var open = function open(ev) {
				// if the overlay is already open
				if (options._open) {
					// debugging notifications
					console.info("Alert is already open");

					// bail out as we don't want to try and re-show the alert
					return;
				}

				// make sure that the active overlay is at the bottom of the document
				// doing this will let the browser do the z-sorting for us.
				document.body.appendChild(popup);

				// create a timeout that immediately executes to allow for animatios
				// to be applied after the alert had been moved to the bottom of the body
				setTimeout(function () {
					// set note the state
					options._open = true;

					// add the active class to the popup
					_dom.dom.addClass(popup, params.active_class);

					// if we shoudl show the background
					if (options.bg) {
						_dom.dom.addClass(document.body, 'noscroll');

						// TODO: potentially do this differently
						_dom.dom.addClass(document.getElementById(params.background_id), params.active_class);
					}

					// if there is a show timer...
					if (options.time) {
						// TODO: this properly
						var bar = new progress({
							target: popup
						});
						var start = new Date().getTime();

						alert_interval = setInterval(function () {
							var diff = new Date().getTime() - start;
							var percent = diff / options.time * 100;

							if (percent >= 100) {
								percent = 100;

								close();
							}

							bar.go(percent);
						}, 100);
					}
				}, 0);
			};

			// get all the elements in the popup that trigger the close event
			var closes = popup.querySelectorAll('[data-close]');

			// loop through all the close triggers, and attach the close function
			[].forEach.call(closes, function (item) {
				_events.events.on(item, 'click', close);
			});

			// if there is a delay on the popup, set a timer to show it
			if (options.delay !== false) {
				setTimeout(open, options.delay);
			}

			// update the operlay to note that it has been initialized, ideally
			// this can be avoided by calling the init method with a specific root element
			setConfig(params.data_name, popup, { _init: true });

			// if there is an id on the alert, add a action to allow it to be called
			if (popup.id) {
				_EventManager2.default.add_action(params.data_name + "#" + popup.id, open);
			}
		});
	}
};

exports.default = popup;

},{"../../libs/events/EventManager":5,"../dom/dom":2,"../events/events":3,"../progressbar/progressbar":8}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

// https://github.com/toddmotto/lunar/blob/master/src/lunar.js

// http://gomakethings.com/climbing-up-and-down-the-dom-tree-with-vanilla-javascript/

/*
var observeDOM = function(elm, callback)
{
	if (MutationObserver)
	{
		var obs = new MutationObserver(function(mutations, observer)
		{
			if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
			{
				callback();
			}
		});

		obs.observe(elm, {
			childList	: true,
			subtree		: true
		});
	}
	else if (eventListenerSupported)
	{
		elm.addEventListener('DOMNodeInserted', callback, false);
		elm.addEventListener('DOMNodeRemoved', callback, false);
	}
};

*/

/*
	toDOM : function(string)
	{
		var d	= document,
			i,
			a	= d.createElement("div"),
			b	= d.createDocumentFragment();

		a.innerHTML	= string;

		while(i = a.firstChild)
		{
			b.appendChild(i);
		}

		return b;
	},
	*/
/*
	createElement : function(options)
	{
		var el, a, i;

		if (!options.tagName)
		{
			el = document.createDocumentFragment();
		}
		else
		{
			el = document.createElement(options.tagName);

			if (options.className)
			{
				el.className = options.className;
			}

			if (options.attributes)
			{
				for (a in options.attributes)
				{
					el.setAttribute(a, options.attributes[a]);
				}
			}

			if (options.html !== undefined)
			{
				el.innerHTML = options.html;
			}
		}

		if (options.text)
		{
			el.appendChild(document.createTextNode(options.text));
		}

		// IE 8 doesn"t have HTMLElement
		if (window.HTMLElement === undefined)
		{
			window.HTMLElement	= Element;
		}

		if (options.childs && options.childs.length)
		{
			for (i = 0; i < options.childs.length; i++)
			{
				el.appendChild(options.childs[i] instanceof window.HTMLElement ? options.childs[i] : createElement(options.childs[i]))
			}
		}

		return el;
	},
	*/

var closest = function () {
	var el = HTMLElement.prototype;
	var matches = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;

	return function closest(el, selector) {
		try {
			return matches.call(el, selector) ? el : closest(el.parentElement, selector);
		} catch (e) {
			return false;
		}
	};
}();

var dom = {
	getPageScroll: function getPageScroll() {
		if (window.pageYOffset) {
			return window.pageYOffset;
		}

		if (document.documentElement && document.documentElement.scrollTop) {
			return document.documentElement.scrollTop;
		}

		if (document.body) {
			return document.body.scrollTop;
		}

		return false;
	},

	position: function position(elm) {
		var box = elm.getBoundingClientRect();

		var body = document.body;
		var docEl = document.documentElement;

		var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
		var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

		var clientTop = docEl.clientTop || body.clientTop || 0;
		var clientLeft = docEl.clientLeft || body.clientLeft || 0;

		var top = box.top + scrollTop - clientTop;
		var left = box.left + scrollLeft - clientLeft;

		return {
			top: Math.round(top),
			left: Math.round(left)
		};
	},

	height: function height(elm) {
		return elm.clientHeight;
	},

	/**
  * Remove an element from the DOM
  * @param  {Element} elm [description]
  */
	remove: function remove(elm) {
		elm.parentNode.removeChild(elm);
	},

	/**
  * [after description]
  * @param  {Element} elm		[description]
  * @param  {[type]} htmlString [description]
  */
	after: function after(elm, htmlString) {
		elm.insertAdjacentHTML('afterend', htmlString);
	},

	/**
  * [insertAfter description]
  * @param  {Element} elm	 [description]
  * @param  {[type]} newNode [description]
  */
	insertAfter: function insertAfter(elm, newNode) {
		elm.parentNode.insertBefore(newNode, elm.nextSibling);
	},

	/**
  * [append description]
  * @param  {Element} elm	 [description]
  * @param  {[type]} newNode [description]
  */
	append: function append(elm, newNode) {
		elm.appendChild(newNode);
	},

	/**
  * [prepend description]
  * @param  {Element} elm	 [description]
  * @param  {[type]} newNode [description]
  */
	prepend: function prepend(elm, newNode) {
		elm.insertBefore(newNode, elm.firstChild);
	},

	/**
  * [before description]
  * @param  {Element} elm		[description]
  * @param  {[type]} htmlString [description]
  */
	before: function before(elm, htmlString) {
		elm.insertAdjacentHTML('beforebegin', htmlString);
	},

	/**
  * [clone description]
  * @param  {Element} elm [description]
  * @return {[type]}	 [description]
  */
	clone: function clone(elm) {
		return elm.cloneNode(true);
	},

	/**
  * [next description]
  * @param  {[type]}   elm [description]
  * @return {Function}	 [description]
  */
	next: function next(elm) {
		return elm.nextElementSibling;
	},

	/**
  * [find description]
  * @param  {Element} elm	  [description]
  * @param  {[type]} selector [description]
  * @return {[type]}		  [description]
  */
	find: function find(elm, selector) {
		return elm.querySelectorAll(selector);
	},

	/**
  * [prev description]
  * @param  {Element} elm [description]
  * @return {[type]}	 [description]
  */
	prev: function prev(elm) {
		return elm.previousElementSibling;
	},

	/**
  * [selectAll description]
  * @param  {[type]} query [description]
  * @return {[type]}	   [description]
  */
	selectAll: function selectAll(query) {
		var base = arguments.length <= 1 || arguments[1] === undefined ? document : arguments[1];

		return base.querySelectorAll(query);
	},

	/**
  * [scrollPosition description]
  * @return {[type]} [description]
  */
	scrollPosition: function scrollPosition() {
		var doc = document.documentElement;

		return {
			top: (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0),
			left: (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0)
		};
	},

	/**
  * [create description]
  * @param  {[type]} str	  [description]
  * @param  {[type]} location [description]
  * @return {[type]}		  [description]
  */
	create: function create(str, location) {
		var temp = document.createElement('div');
		temp.innerHTML = str;

		var children;

		if (!location) {
			children = temp;
		} else {
			children = temp.children;

			arr.each(location, function (str, i, name) {
				children = i == location.length - 1 ? children[str] : children[str].children;
			});
		}

		return children;
	},

	containsClass: function () {
		if (!!document.documentElement.classList) {
			/**
    * [description]
    * @param  {Element} elm	   [description]
    * @param  {[type]} className [description]
    */
			return function (elm, className) {
				if (!elm) {
					return false;
				}

				return elm.classList.contains(className);
			};
		}

		/**
   * [description]
   * @param  {Element} elm	   [description]
   * @param  {[type]} className [description]
   */
		return function (elm, className) {
			if (!elm || !elm.className) {
				return false;
			}

			var re = new RegExp('(^|\\s)' + className + '(\\s|$)');

			return elm.className.match(re);
		};
	}(),

	addClass: function () {
		if (!!document.documentElement.classList) {
			/**
    * [description]
    * @param  {Element} elm	   [description]
    * @param  {[type]} className [description]
    */
			return function (elm, className) {
				if (!elm) {
					return false;
				}

				if (className.indexOf(' ') > 0) {
					var names = className.split(' ');

					for (var i = 0, il = names.length; i < il; i++) {
						elm.classList.add(names[i]);
					}
				} else {
					elm.classList.add(className);
				}
			};
		}

		/**
   * [description]
   * @param  {Element} elm	   [description]
   * @param  {[type]} className [description]
   */
		return function (elm, className) {
			if (!elm) {
				return false;
			}

			if (!this.containsClass(elm, className)) {
				elm.className += (elm.className ? ' ' : '') + className;
			}
		};
	}(),

	removeClass: function () {
		if (!!document.documentElement.classList) {
			/**
    * [description]
    * @param  {Element} elm	   [description]
    * @param  {[type]} className [description]
    */
			return function (elm, className) {
				if (!elm) {
					return false;
				}

				elm.classList.remove(className);
			};
		}

		/**
   * [description]
   * @param  {Element} elm	   [description]
   * @param  {[type]} className [description]
   */
		return function (elm, className) {
			if (!elm || !elm.className) {
				return false;
			}

			var regexp = new RegExp('(^|\\s)' + className + '(\\s|$)', 'g');

			elm.className = elm.className.replace(regexp, '$2');
		};
	}(),

	toggleClass: function () {
		if (!!document.documentElement.classList) {
			/**
    * [description]
    * @param  {Element} elm	   [description]
    * @param  {[type]} className [description]
    */
			return function (elm, className) {
				if (!elm) {
					return false;
				}

				return elm.classList.toggle(className);
			};
		}

		/**
   * [description]
   * @param  {Element} elm	   [description]
   * @param  {[type]} className [description]
   */
		return function (elm, className) {
			if (!elm) {
				return false;
			}

			if (this.containsClass(elm, className)) {
				this.removeClass(elm, className);
				return false;
			} else {
				this.addClass(elm, className);
				return true;
			}
		};
	}(),

	/**
  * Get the closest matching element up the DOM tree.
  * @private
  * @param  {Element} elem	 Starting element
  * @param  {String}  selector Selector to match against (class, ID, data attribute, or tag)
  * @return {Boolean|Element}  Returns null if not match found
  */
	getClosest: function getClosest(elm, selector) {
		// Variables
		var firstChar = selector.charAt(0);
		var supports = 'classList' in document.documentElement;

		var attribute, value;

		// If selector is a data attribute, split attribute from value
		if (firstChar === '[') {
			selector = selector.substr(1, selector.length - 2);
			attribute = selector.split('=');

			if (attribute.length > 1) {
				value = true;
				attribute[1] = attribute[1].replace(/"/g, '').replace(/'/g, '');
			}
		}

		// Get closest match
		for (; elm && elm !== document; elm = elm.parentNode) {

			// If selector is a class
			if (firstChar === '.') {
				if (supports) {
					if (elm.classList.contains(selector.substr(1))) {
						return elm;
					}
				} else {
					if (new RegExp('(^|\\s)' + selector.substr(1) + '(\\s|$)').test(elm.className)) {
						return elm;
					}
				}
			}

			// If selector is an ID
			if (firstChar === '#') {
				if (elm.id === selector.substr(1)) {
					return elm;
				}
			}

			// If selector is a data attribute
			if (firstChar === '[') {
				if (elm.hasAttribute(attribute[0])) {
					if (value) {
						if (elm.getAttribute(attribute[0]) === attribute[1]) {
							return elm;
						}
					} else {
						return elm;
					}
				}
			}

			// If selector is a tag
			if (elm.tagName.toLowerCase() === selector) {
				return elm;
			}
		}

		return null;
	},

	closest: closest
};

exports.dom = dom;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.events = undefined;

var _dom = require('../dom/dom');

/**
 * Events module
 * @module events
 * @global
 */
var events = {

	/**
  * Trigger a function when the document is ready
  * @memberOf module:events#
  * @param  {Function} fn Function to call when ready
  */
	ready: function ready(fn) {
		// if the document is already ready... caall the function
		if (document.readyState != 'loading') {
			fn();
		}
		// add the loaded event listener
		else {
				this.on(document, 'DOMContentLoaded', fn);
			}
	},

	/**
  * [live description]
  * @param  {[type]}   selector [description]
  * @param  {[type]}   evt	  [description]
  * @param  {Function} fn	   [description]
  * @param  {[type]}   scope	[description]
  * @return {[type]}			[description]
  */
	live: function live(selector, evt, fn, elementScope) {
		return this.on(elementScope || document, evt, function (ev) {
			var listeningTarget = _dom.dom.closest(ev.target, selector);

			if (listeningTarget) {
				fn.call(listeningTarget, ev);
			}
		});
	},

	/**
 * Bind event to element
 * @memberOf module:events#
 * @param {object} el DOM element
 * @param {string} evt Event type
 * @param {function} fn Callback
 */
	on: function on(el, evt, fn) {
		if (!el) {
			return false;
		}

		if (el.addEventListener) {
			this.addEvent = function (el, evt, fn) {
				el.addEventListener(evt, fn, false);
				return el;
			};
		} else if (el.attachEvent) {
			this.addEvent = function (el, evt, fn) {
				el.attachEvent('on' + evt, fn);
				return el;
			};
		} else {
			this.addEvent = function (el, evt, fn) {
				el['on' + evt] = fn;
				return el;
			};
		}

		return this.addEvent(el, evt, fn);
	},

	/**
 * Remove event to element
 * @memberOf module:events#
 * @param {object} el DOM element
 * @param {string} evt Event type
 * @param {function} fn Callback
 */
	off: function off(el, evt, fn) {
		if (!el) {
			return false;
		}

		if (el.removeEventListener) {
			this.removeEvent = function (el, evt, fn) {
				el.removeEventListener(evt, fn, false);
				return el;
			};
		} else if (el.detachEvent) {
			this.removeEvent = function (el, evt, fn) {
				el.removeEvent('on' + evt, fn);
				return el;
			};
		} else {
			this.removeEvent = function (el, evt, fn) {
				el['on' + evt] = fn;
				return el;
			};
		}
		return this.removeEvent(el, evt, fn);
	},

	/**
 * Trigger event on element
 * @memberOf module:events#
 * @param {object} el DOM element
 * @param {string} evt Event type
 * @param {function} fn Callback
 */
	trigger: function trigger(el, evt) {
		if (!el) {
			return false;
		}

		var _evt;

		if (document.createEvent) {
			_evt = document.createEvent('HTMLEvents');
			_evt.initEvent(evt, true, true);
		} else {
			_evt = document.createEventObject();
			_evt.eventType = evt;
		}

		_evt.eventName = evt;

		if (document.createEvent) {
			el.dispatchEvent(_evt);
		} else {
			el.fireEvent('on' + _evt.eventType, _evt);
		}
	},

	/**
 * Cross browser e.preventDefault()
 * @memberOf module:events#
 * @param {string} evt Event type
 */
	cancel: function cancel(evt) {
		if (!evt) {
			return false;
		}

		if (evt.stopPropagation) {
			evt.stopPropagation();
		}

		if (evt.preventDefault) {
			evt.preventDefault();
		} else {
			evt.returnValue = false;
		}
	}
};

exports.events = events;

},{"../dom/dom":2}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _dom = require('../dom/dom');

var _events = require('../events/events');

var _Loader = require('../request/Loader');

var _Loader2 = _interopRequireDefault(_Loader);

var _EventManager = require('../events/EventManager');

var _EventManager2 = _interopRequireDefault(_EventManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// https://bitsofco.de/accessible-modal-dialog/?utm_source=codropscollective
// https://charlgottschalk.github.io/approvejs/

_EventManager2.default.add_action('page-refresh', function (options, element) {
	var defaults = {
		delay: 0,
		url: false
	};

	options = Object.assign(defaults, options);

	setTimeout(function () {
		if (options.url) {
			alert(options.url);
			window.location = options.url;
		} else {
			location.reload(true);
		}
	}, options.delay);
}, 10, 2);

function serialize(form) {
	if (!form || form.nodeName !== "FORM") {
		return;
	}

	var i,
	    j,
	    q = [];

	for (i = form.elements.length - 1; i >= 0; i = i - 1) {
		if (form.elements[i].name === "") {
			continue;
		}
		switch (form.elements[i].nodeName) {
			case 'INPUT':
				switch (form.elements[i].type) {
					case 'text':
					case 'hidden':
					case 'password':
					case 'button':
					case 'reset':
					case 'submit':
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						break;
					case 'checkbox':
					case 'radio':
						if (form.elements[i].checked) {
							q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						}
						break;
					case 'file':
						break;
				}
				break;

			case 'TEXTAREA':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				break;

			case 'SELECT':
				switch (form.elements[i].type) {
					case 'select-one':
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						break;

					case 'select-multiple':
						for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
							if (form.elements[i].options[j].selected) {
								q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
							}
						}
						break;
				}
				break;

			case 'BUTTON':
				switch (form.elements[i].type) {
					case 'reset':
					case 'submit':
					case 'button':
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
						break;
				}
				break;
		}
	}
	return q.join("&");
}

var ajax = {
	init: function init() {
		var root = arguments.length <= 0 || arguments[0] === undefined ? document : arguments[0];

		// TODO, possibly allow the option to decide to ajax content or not...
		_events.events.live('form', 'submit', function (ev) {
			_events.events.cancel(ev);

			//
			//
			// TODO - optional form validation
			//
			//

			var form = this;

			// serialize the form data
			var data = serialize(form);

			// create an instance of the loader to make the ajax request
			var l = new _Loader2.default();

			// select all the buttons in the dom that are currently not disabled
			var buttons = form.querySelectorAll('button:not(disabled), input[type="submit"]:not(disabled)');

			// loop through all the buttons and disable them so the form cannot be submitted multiple times
			[].forEach.call(buttons, function (button, i) {
				// disable the button
				button.disabled = true;

				// if the button is the last button on the form
				if (i === buttons.length - 1) {
					// set its state to loading
					_dom.dom.addClass(button, "btn--loading");
				}
			});

			// select all the inputs that are not disabled to make sure that they cannot be interacted with while ajaxing...
			var inputs = form.querySelectorAll('input:not(disabled), textarea:not(disabled), select:not(disabled)');

			// loop through all the inputs and dissble them
			[].forEach.call(inputs, function (input, i) {
				input.disabled = true;
			});

			// post the form data and watch for events
			l.post(form.action, data).success(
			// success function
			function (data, xhr) {
				try {
					// create a hidden element to hold all the
					var hidden = document.body.appendChild(document.createElement("div")),
					    re = new RegExp(/<body[^>]*>([\s\S]+)<\/body>/i),
					    elem;

					// if the data has the body tag...
					if (data.match(re)) {
						// splut and return the contents of the body
						data = data.match(re)[1];
					}

					// hide the hidden container
					hidden.style.display = 'none';

					// add the newly loaded ajax data into the hidden element so we can query it
					hidden.innerHTML = data;

					// EventManager.do_action('init_content', hidden);

					// forms should have an id, so elements can be matched up
					elem = hidden.querySelector('#' + form.id);

					// replace the current form content with the ajaxed in content (might not be a <form>), if this fails, page will auto-refresh
					form.parentNode.replaceChild(elem, form);

					// if the element can be found
					if (elem) {
						try {
							// get data attribute foom the returned element
							var attr = elem.getAttribute('data-response');

							// parse the settings as json
							var options = JSON.parse(attr);

							// loop through all the actions and call them..
							if (options.action) {
								for (var i in options.action) {
									console.log(i, options.action[i]);

									//EventManager.do_action(i, options.action[i], elem);
								}
							}
						} catch (e) {}
					}
				}
				// if anything fails, just refresh the page as a precaution, this may happen
				// for pages like login where a redirect is in place
				catch (e) {
					alert("ajax.js fail");
					location.reload(true);
				}
			}).error(
			// error handler, this should only be hit on a server error
			function (data, xhr) {
				// TODO - this is a fatal error...
				console.log('-> error', arguments);
			}).always(
			// regardless of the result, this will always get executed
			function (data, xhr) {
				// always reenable the buttons to the user can continue
				[].forEach.call(buttons, function (button, i) {
					// re-enable the button
					button.disabled = false;

					// remove the loading indicator
					_dom.dom.removeClass(button, "btn--loading");
				});

				// loop through all the inputs
				[].forEach.call(inputs, function (input, i) {
					// re-enable the input...
					input.disabled = false;
				});
			});
		});
	}
};

exports.default = ajax;

},{"../dom/dom":2,"../events/events":3,"../events/EventManager":5,"../request/Loader":9}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var EventManager = {
	/**
  * Toggle debugging on and off
  * @param  {Boolean} bool [description]
  */
	debug: function debug() {
		var bool = arguments.length <= 0 || arguments[0] === undefined ? true : arguments[0];

		window._evm = bool;
	},

	/**
  * [create description]
  * @param  {String}  type		  [description]
  * @param  {String}  hook		  [description]
  * @param  {Ibject}  func		  [description]
  * @param  {Integer} priority	  [description]
  * @param  {Integer} accepted_args [description]
  */
	_create: function _create(type, hook, func) {
		var priority = arguments.length <= 3 || arguments[3] === undefined ? 10 : arguments[3];
		var accepted_args = arguments.length <= 4 || arguments[4] === undefined ? 1 : arguments[4];

		type = '_' + type;

		if (!window[type]) {
			window[type] = {};
		}

		if (!window[type][hook]) {
			window[type][hook] = [];
		}

		if (!window[type][hook][priority]) {
			window[type][hook][priority] = [];
		}

		window[type][hook][priority].push({ func: func, args: accepted_args });
	},

	/**
  * [add_filter description]
  * @param  {String}  hook		  [description]
  * @param  {Object}  func		  [description]
  * @param  {Integer} priority	  [description]
  * @param  {Integer} accepted_args [description]
  */
	add_action: function add_action(hook, func) {
		var priority = arguments.length <= 2 || arguments[2] === undefined ? 10 : arguments[2];
		var accepted_args = arguments.length <= 3 || arguments[3] === undefined ? 1 : arguments[3];

		this._create('action', hook, func, priority, accepted_args);
	},

	/**
  * [add_filter description]
  * @param  {String}  hook		  [description]
  * @param  {Object}  func		  [description]
  * @param  {Integer} priority	  [description]
  * @param  {Integer} accepted_args [description]
  */
	add_filter: function add_filter(hook, func) {
		var priority = arguments.length <= 2 || arguments[2] === undefined ? 10 : arguments[2];
		var accepted_args = arguments.length <= 3 || arguments[3] === undefined ? 1 : arguments[3];

		this._create('filter', hook, func, priority, accepted_args);
	},

	/**
  * [_do description]
  * @param  {[type]} type [description]
  * @param  {[type]} hook [description]
  * @return {[type]}	  [description]
  */
	_do: function _do(type, hook) {
		var value = arguments[2];

		if (window[type]) {
			var list = window[type][hook];

			if (window._evm === true) {
				console.log('-> Running ' + type + ':', hook);
			}

			if (list) {
				for (var i in list) {
					// get the list of functions
					var functions = list[i];

					for (var j in functions) {
						var func = functions[j].func;
						var target = null;

						if (func instanceof Array) {
							target = func[0];
							func = func[1];

							if (typeof func === 'string') {
								func = target[func];
							}
						}

						if (window._evm === true) {
							console.log('   [func]', func.name);
						}

						var args = Array.prototype.slice.call(arguments, 2, functions[j].args + 2);
						args[0] = value;

						value = func.apply(target, args);
					}
				}
			}
		}

		return value;
	},

	/**
  * [do_action description]
  * @param  {String}  hook		  [description]
  */
	do_action: function do_action(hook) {
		var args = Array.prototype.slice.call(arguments);
		args.unshift('_action');

		return this._do.apply(this, Array.prototype.slice.call(args));
	},

	/**
  * [do_action description]
  * @param  {String}  hook		  [description]
  */
	apply_filters: function apply_filters(hook) {
		var args = Array.prototype.slice.call(arguments);
		args.unshift('_filter');

		return this._do.apply(this, Array.prototype.slice.call(args));
	}
};

exports.default = EventManager;

},{}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _dom = require('../dom/dom');

var _events = require('../events/events');

var _Loader = require('../request/Loader');

var _Loader2 = _interopRequireDefault(_Loader);

var _EventManager = require('../../libs/events/EventManager');

var _EventManager2 = _interopRequireDefault(_EventManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// https://bitsofco.de/accessible-modal-dialog/?utm_source=codropscollective
// https://charlgottschalk.github.io/approvejs/

var overlay = {
	init: function init() {
		var root = arguments.length <= 0 || arguments[0] === undefined ? document : arguments[0];

		var OVERLAY_INACTIVE = 'overlay--inactive';
		var OVERLAY_ACTIVE = 'active';
		var OVERLAY_OPEN = 'overlay--open';

		// global
		var overlayHolder = document.querySelector('#overlays');

		if (!overlayHolder) {
			return false;
		}

		var get_id = function get_id(string) {
			string = string.toLowerCase();

			// TODO - add more
			return string;
		};

		var template = overlayHolder.innerHTML;
		var pages = [];
		var pages_open = [];
		var lookups = {};

		overlayHolder.innerHTML = '';

		var position_overlays = function position_overlays() {
			// TODO..
			[].forEach.call(pages_open, function (id, i) {
				var page = pages[id];

				// offset to stagger the overlays
				var offset = (pages_open.length - i - 1) * 30;
				var is_active = i === pages_open.length - 1;

				if (!is_active) {
					_dom.dom.addClass(page, OVERLAY_INACTIVE);
				} else {
					_dom.dom.removeClass(page, OVERLAY_INACTIVE);
				}

				// add the margin to the overlay
				page.style.marginRight = offset + 'px';
			});
		};

		var open_overlay = function open_overlay(key) {
			// add the new page into the pages open array
			pages_open.push(key);

			// get the overlay from the pages
			var overlay = pages[key];
			overlay.style.zIndex = pages_open.length + 1;

			setTimeout(function () {
				position_overlays();

				_dom.dom.addClass(overlayHolder, OVERLAY_ACTIVE);
				_dom.dom.addClass(overlay, OVERLAY_OPEN);

				/*
    		// TODO - implement
    // make the modal/overlay accessible
    var focusable_els		= overlay.querySelectorAll('a[href]:not(.ui), area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), [tabindex="0"]');
    	focusable_els		= Array.prototype.slice.call(focusable_els);
    		if(focusable_els.length)
    {
    	// TODO - this breaks functionality
    	//focusable_els[0].focus();
    }
    		*/
			}, 100);

			_dom.dom.addClass(document.body, 'noscroll');
		};

		var create_overlay = function create_overlay(xhr, el, url) {
			// create an overlay wrapper from the template html already embedded in the page
			var overlay = _dom.dom.create(template).children[0];

			// find the template body
			var body = _dom.dom.find(overlay, '.overlay__body');

			el.setAttribute('aria-hidden', false);

			// add the newly loaded html into the template
			_dom.dom.append(body[0], el);

			var overlay_title = el.getAttribute('data-title');
			var title = _dom.dom.find(overlay, '.overlay__title');
			title[0].innerHTML = overlay_title;

			// add the overlay into the overlay holder
			_dom.dom.append(overlayHolder, overlay);

			// add the newly loaded overlay into the pages
			pages.push(overlay);

			_EventManager2.default.do_action('content-loaded', overlay);

			// what possition was the content added to the pages array
			var position = pages.length - 1;

			// get the id from the url, so we can update the lookups data
			var id = get_id(url);

			// update the lookups with a position
			lookups[id] = position;

			// open the overlay...
			open_overlay(position);
		};

		var load = function load(url, callback) {
			var parts = url.split('#');
			var full_url = url;

			getDOM('#' + parts[1], parts[0], function (xhr, el, url) {
				create_overlay(xhr, el, full_url);

				if (callback) {
					callback.call();
				}
			}, function (xhr, el, url) {
				window.location = xhr.responseURL;
			}, function () {
				// progress
			});
		};

		var getDOM = function getDOM(selector, url, success, fail, progress) {
			var _loader = new _Loader2.default();

			// set up success and error events
			_loader.get(url).success(function (data, xhr) {
				var hidden = document.body.appendChild(document.createElement("div")),
				    re = new RegExp(/<body[^>]*>([\s\S]+)<\/body>/i),
				    elem;

				hidden.style.display = 'none';
				hidden.innerHTML = data.match(re)[1];

				elem = hidden.querySelector(selector);

				// TODO - pull any alerts out from the
				console.info("insert additional alerts/messages etc...");

				if (true) {
					success(xhr, elem, url);
				} else {
					alert("FAIL");
				}

				document.body.removeChild(hidden);
			}).error(function (data, xhr) {
				console.log('-> error', arguments);
			}).always(function (data, xhr) {
				//console.log('-> always', arguments);
			});
		};

		var _open = function _open(href) {
			// get an id to use as a lookup
			var id = get_id(href);

			// if the overlay has already been created... there's no point in generating another overlay
			if (lookups[id] !== undefined) {
				// if the item has a state rather than an id number...
				if (typeof lookups[id] === 'string') {
					console.warn("STATE:" + lookups[id]);
				} else {
					// open the correct overlay
					open_overlay(lookups[id]);
				}
			} else {
				// set a loading stare for the overay, as it hasn't yet been created
				lookups[id] = 'loading';

				// if the overlay is a link to an existing element on the page
				if (href.charAt(0) === '#') {
					var el = document.querySelector(href);

					if (el) {
						create_overlay(null, el, href);
					} else {
						alert("inPage overlay not available...");
					}
				} else {
					load(href);
				}
			}
		};

		var open = function open(ev) {
			if (!ev.ctrlKey) {
				_events.events.cancel(ev);

				// get the anchor url
				_open(this.getAttribute('href'));
			}
		};

		var close = function close(ev) {
			_events.events.cancel(ev);

			var key = pages_open.pop();
			var overlay = pages[key];

			_dom.dom.removeClass(overlay, OVERLAY_OPEN);

			if (pages_open.length === 0) {
				_dom.dom.removeClass(overlayHolder, OVERLAY_ACTIVE);

				setTimeout(function () {
					_dom.dom.removeClass(document.body, 'noscroll');
				}, 10);
			}

			position_overlays();
		};

		var closeAll = function closeAll(ev) {
			_events.events.cancel(ev);

			for (var i = 0; i < pages_open.length; i++) {
				var key = pages_open.pop();
				var overlay = pages[key];

				_dom.dom.removeClass(overlay, OVERLAY_OPEN);

				position_overlays();
			}

			if (pages_open.length === 0) {
				_dom.dom.removeClass(overlayHolder, 'overlays--active');
				_dom.dom.removeClass(document.body, 'noscroll');
			}
		};

		_EventManager2.default.add_action('close-overlay', function (options, element) {
			var defaults = {
				delay: 0
			};

			options = Object.assign(defaults, options);

			setTimeout(function () {
				close();
			}, options.delay);
		}, 10, 2);

		_EventManager2.default.add_action('open-overlay', function (options, element) {
			var defaults = {
				path: '',
				delay: 0
			};

			options = Object.assign(defaults, options);

			setTimeout(function () {
				_open(options.path);
			}, options.delay);
		}, 10, 2);

		//
		_events.events.live('a.js-overlay-o', 'click', open);
		_events.events.live('a.js-overlay-c', 'click', close);
		_events.events.live('a.js-overlay-a', 'click', closeAll);

		_events.events.on(overlayHolder, 'click', function (ev) {
			if (ev.target === overlayHolder) {
				close(ev);
			}
		});

		if (window.location.hash) {
			var elements = document.querySelectorAll('a[href="' + window.location.hash + '"]');

			[].forEach.call(elements, function (link, i) {
				if (_dom.dom.containsClass(link, 'js-overlay-o')) {
					link.click();
				}
			});
		}
	}
};

exports.default = overlay;

},{"../../libs/events/EventManager":5,"../dom/dom":2,"../events/events":3,"../request/Loader":9}],7:[function(require,module,exports){
'use strict';

var _events = require('../../events/events');

var _dom = require('../../dom/dom');

var _EventManager = require('../../events/EventManager');

var _EventManager2 = _interopRequireDefault(_EventManager);

var _overlay = require('../overlay');

var _overlay2 = _interopRequireDefault(_overlay);

var _ajax = require('../../form/ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _alert = require('../../alert/alert');

var _alert2 = _interopRequireDefault(_alert);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_EventManager2.default.add_action('init_content', [_alert2.default, 'init']);
_EventManager2.default.add_action('init_content', [_overlay2.default, 'init']);
_EventManager2.default.add_action('init_content', [_ajax2.default, 'init']);

_events.events.ready(function () {
	_EventManager2.default.do_action('init_content');
});

},{"../../alert/alert":1,"../../dom/dom":2,"../../events/events":3,"../../form/ajax":4,"../../events/EventManager":5,"../overlay":6}],8:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

/* http://nanobar.micronube.com/  ||  https://github.com/jacoborus/nanobar/    MIT LICENSE */
(function (root) {
    'use strict';
    // container styles

    var css = '.nanobar{width:100%;height:4px;z-index:9999;top:0}.bar{width:0;height:100%;transition:height .3s;background:#000}';

    // add required css in head div
    function addCss() {
        var s = document.getElementById('nanobarcss');

        // check whether style tag is already inserted
        if (s === null) {
            s = document.createElement('style');
            s.type = 'text/css';
            s.id = 'nanobarcss';
            document.head.insertBefore(s, document.head.firstChild);
            // the world
            if (!s.styleSheet) return s.appendChild(document.createTextNode(css));
            // IE
            s.styleSheet.cssText = css;
        }
    }

    function addClass(el, cls) {
        if (el.classList) el.classList.add(cls);else el.className += ' ' + cls;
    }

    // create a progress bar
    // this will be destroyed after reaching 100% progress
    function createBar(rm) {
        // create progress element
        var el = document.createElement('div'),
            width = 0,
            here = 0,
            on = 0,
            bar = {
            el: el,
            go: go
        };

        addClass(el, 'bar');

        // animation loop
        function move() {
            var dist = width - here;

            if (dist < 0.1 && dist > -0.1) {
                place(here);
                on = 0;
                if (width === 100) {
                    el.style.height = 0;
                    setTimeout(function () {
                        rm(el);
                    }, 300);
                }
            } else {
                place(width - dist / 4);
                setTimeout(go, 16);
            }
        }

        // set bar width
        function place(num) {
            width = num;
            el.style.width = width + '%';
        }

        function go(num) {
            if (num >= 0) {
                here = num;
                if (!on) {
                    on = 1;
                    move();
                }
            } else if (on) {
                move();
            }
        }
        return bar;
    }

    function Progressbar(opts) {
        opts = opts || {};
        // set options
        var el = document.createElement('div'),
            applyGo,
            nanobar = {
            el: el,
            go: function go(p) {
                // expand bar
                applyGo(p);
                // create new bar when progress reaches 100%
                if (p === 100) {
                    init();
                }
            }
        };

        // remove element from nanobar container
        function rm(child) {
            el.removeChild(child);
        }

        // create and insert progress var in nanobar container
        function init() {
            var bar = createBar(rm);
            el.appendChild(bar.el);
            applyGo = bar.go;
        }

        addCss();

        addClass(el, 'nanobar');
        if (opts.id) el.id = opts.id;
        if (opts.classname) addClass(el, opts.classname);

        // insert container
        if (opts.target) {
            // inside a div
            el.style.position = 'relative';
            opts.target.insertBefore(el, opts.target.firstChild);
        } else {
            // on top of the page
            el.style.position = 'fixed';
            document.getElementsByTagName('body')[0].appendChild(el);
        }

        init();
        return nanobar;
    }

    if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
        // CommonJS
        module.exports = Progressbar;
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], function () {
            return Progressbar;
        });
    } else {
        // Browser globals
        root.Progressbar = Progressbar;
    }
})(undefined);

},{}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://github.com/jacoborus/nanobar/blob/master/nanobar.js

// import {dom} from '../dom/dom';

var Loader = function () {
	function Loader() {
		var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

		_classCallCheck(this, Loader);

		var defaults = {
			contentType: 'application/x-www-form-urlencoded',
			cachebust: false,
			show_progress: true
		};

		this.config = Object.assign(defaults, config);

		return this;
	}

	_createClass(Loader, [{
		key: 'parse',
		value: function parse(req) {
			var result;

			try {
				result = JSON.parse(req.responseText);
			} catch (e) {
				result = req.responseText;
			}

			return [result, req];
		}
	}, {
		key: 'xhr',
		value: function xhr(type, url, data) {
			if (!url) {
				throw "Url is not defined";
			}

			if (this.config.cachebust) {
				url += url.indexOf('?') > -1 ? '' : '?s=' + Date.now();
			}

			var methods = {
				success: function success() {},
				error: function error() {},
				always: function always() {}
			};

			var XHR = XMLHttpRequest || ActiveXObject;
			var request = new XHR('MSXML2.XMLHTTP.3.0');
			var self = this;

			request.open(type, url, true);

			request.setRequestHeader('Content-type', this.config.contentType);
			request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest');

			if (self.config.show_progress) {
				console.log("SHOW PROGRESS");

				//var bar				= dom.create('<div class="page-loader"></div>');
				//	bar				= bar.firstChild;

				//document.body.appendChild(bar);
			}

			request.onreadystatechange = function () {
				var req;

				if (request.readyState === 4) {
					req = self.parse(request);

					if (request.status >= 200 && request.status < 300) {
						methods.success.apply(methods, req);
					} else {
						methods.error.apply(methods, req);
					}

					if (self.config.show_progress) {
						console.log("KILL PROGRESS");
					}

					methods.always.apply(methods, req);
				}
			};

			// do the request
			request.send(data);

			var obj = {
				success: function success(callback) {
					methods.success = callback;

					return obj;
				},
				error: function error(callback) {
					methods.error = callback;

					return obj;
				},
				always: function always(callback) {
					methods.always = callback;

					return obj;
				}
			};

			return obj;
		}
	}, {
		key: 'get',
		value: function get(src) {
			return this.xhr('GET', src);
		}
	}, {
		key: 'put',
		value: function put(url, data) {
			return this.xhr('PUT', url, data);
		}
	}, {
		key: 'post',
		value: function post(url, data) {
			return this.xhr('POST', url, data);
		}
	}, {
		key: 'delete',
		value: function _delete(url) {
			return this.xhr('DELETE', url);
		}

		// TODO - set content type method

	}]);

	return Loader;
}();

exports.default = Loader;

},{}]},{},[7]);
